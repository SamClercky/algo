pub mod sorting {
    use crate::search::selection_sort::selection_sort;
    use crate::search::bubble_sort::bubble_sort;
    use crate::search::quick_sort::quick_sort;
    use crate::search::merge_sort::*;

    #[test]
    fn test_selection_sort() {
        let array = vec![1, 4, 3, 2];
        assert_eq!(selection_sort(&array), vec![1, 2, 3, 4]);
    }

    #[test]
    fn test_bubble_sort() {
        let mut array = vec![1, 4, 3, 2];
        bubble_sort(&mut array);
        assert_eq!(array, vec![1, 2, 3, 4]);
    }

    #[test]
    fn test_quick_sort() {
        let mut array = vec![51, 3, 24, 86, 45, 30, 27, 63, 96, 50, 10];
        quick_sort(&mut array);
        assert_eq!(array, vec![3, 10, 24, 27, 30, 45, 50, 51, 63, 86, 96]);
    }

    #[test]
    fn test_merge_sort() {
        let array = vec![51, 3, 24, 86, 45, 30, 27, 63, 96, 50, 10];
        assert_eq!(merge_sort(&array), vec![3, 10, 24, 27, 30, 45, 50, 51, 63, 86, 96]);
    }

    #[test]
    fn test_merge_sort_recursief() {
        let array = vec![51, 3, 24, 86, 45, 30, 27, 63, 96, 50, 10];
        assert_eq!(merge_sort_recursive(&array), vec![3, 10, 24, 27, 30, 45, 50, 51, 63, 86, 96]);
    }
}

pub mod datastructures {
    use crate::datastructures::linkedlist::*;

    #[test]
    fn test_linked_list() {
        let mut list = LinkedList::<u32>::new();
        list.push(1);
        list.push(2);
        list.push(3);

        // check length
        assert_eq!(list.len(), 3);

        // check get
        assert_eq!(list.get(0).unwrap(), 1);
        assert_eq!(list.get(1).unwrap(), 2);
        assert_eq!(list.get(2).unwrap(), 3);
    }
}

pub mod common {
    use crate::common::*;

    #[test]
    fn test_common_min() {
        let array = vec![2, 4, 3, 1];
        assert_eq!(min(&array), (&1, 3));
    }

    #[test]
    fn test_common_max() {
        let array = vec![2, 4, 3, 1];
        assert_eq!(max(&array), (&4, 1));
    }
}
