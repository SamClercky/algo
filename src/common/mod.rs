pub fn min<T: PartialOrd>(array: &[T]) -> (&T, usize) {
    let mut curr_min = &array[0];
    let mut curr_index = 0;

    for (i, item) in array.iter().enumerate() {
        if item < curr_min {
            curr_min = item;
            curr_index = i;
        }
    }

    (curr_min, curr_index)
}

pub fn max<T: PartialOrd>(array: &[T]) -> (&T, usize) {
    let mut curr_max = &array[0];
    let mut curr_index = 0;

    for (i, item) in array.iter().enumerate() {
        if item > curr_max {
            curr_max = item;
            curr_index = i;
        }
    }

    (curr_max, curr_index)
}