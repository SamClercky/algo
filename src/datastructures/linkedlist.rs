use std::mem;

pub struct LinkedList<T> {
    first: NodeItem<T>,
}

type NodeItem<T> = Option<Box<Node<T>>>;

pub struct Node<T> {
    data: T,
    next: NodeItem<T>,
}

impl <T> LinkedList<T> {
    pub fn new() -> LinkedList<T> {
        LinkedList { first: None }
    }

    pub fn len(&self) -> usize {
        let mut result = 0;

        let mut current = &self.first;
        while let Some(node) = current {
            current = &node.next;
            result += 1;
        }

        result
    }

    pub fn push(&mut self, item: T) {
        let new_node = Some(Box::new(Node {
            data: item,
            next: None,
        }));

        // Walk the tree
        let mut current = &mut self.first;
        loop {
            match current {
                Some(node) => {
                    if let Some(_) = &mut node.next {
                        current = &mut node.next
                    } else {
                        mem::replace(&mut node.next, new_node);
                        return;
                    }
                }
                None => {
                    mem::replace(current, new_node);
                    return;
                }
            }
        }
    }
}


impl <T: Clone> LinkedList<T> {
    pub fn get(&self, index: usize) -> Option<T> {
        assert!(index < self.len());

        let mut current = &self.first;

        for _ in 0..index {
            if let Some(node) = current {
                current = &node.next;
            }
        }

        if let Some(result) = current {
            return Some((**result).data.clone());
        } else {
            return None;
        }
    }
}