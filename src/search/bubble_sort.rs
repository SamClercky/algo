
///
/// Implementation of bubble sort
///
pub fn bubble_sort<T: PartialOrd>(array: &mut [T]) {
    let mut has_changed = true;

    while has_changed {
        has_changed = false;
        
        for i in 0..(array.len()-1) {
            if !(array[i] < array[i+1]) {
                has_changed = true;
                array.swap(i, i+1);
            }
        }
    }
}