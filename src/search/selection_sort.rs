use crate::common::min;

///
/// Uses selection sort to sort array in place
/// 
pub fn selection_sort<T: PartialOrd + Clone>(array: &[T]) -> Vec<T> {
    let mut temp_array: Vec<T> = array.to_owned();
    let mut result = vec![];

    for _ in array.iter() {
        let (_, min_index) = min(&temp_array);
        let head = temp_array[min_index].clone();
        temp_array.remove(min_index);
        result.push(head);
    }
    
    result
}