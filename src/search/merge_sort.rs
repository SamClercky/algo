
///
/// Merge sort implementation
///
pub fn merge_sort<T: PartialOrd + Clone>(array: &[T]) -> Vec<T> {
    let mut result: Vec<Vec<T>> = vec![];

    // Get sorted parts
    let mut start_index = 0;
    for i in 0..(array.len()) {
        if i == array.len()-1 || array[i] > array[i+1] {
            result.push(Vec::from(&array[start_index..(i+1)]));
            start_index = i+1;
        }
    }

    while result.len() > 1 {
        let first = result.pop().unwrap();
        let second = result.pop().unwrap();

        let merged_lists = merge(&first, &second);

        result.insert(0, merged_lists);
    
    }

    result.pop().unwrap()
}

///
/// Merge sort recursive implementation
///
pub fn merge_sort_recursive<T: PartialOrd + Clone>(array: &[T]) -> Vec<T> {
    let split = array.len() / 2;

    let arr1 = Vec::from(&array[..split]);
    let arr2 = Vec::from(&array[split..]);

    return merge(
        &(if arr1.len() > 1 { merge_sort_recursive(&arr1) } else { arr1 }),
        &(if arr2.len() > 1 { merge_sort_recursive(&arr2) } else { arr2 })
    )
}

fn merge<T: PartialOrd + Clone>(arr1: &[T], arr2: &[T]) -> Vec<T> {
    let mut result: Vec<T> = Vec::with_capacity(arr1.len() + arr2.len());

    let mut i = 0;
    let mut j = 0;

    while i < arr1.len() && j < arr2.len() {
        if arr1[i] < arr2[j] {
            result.push(arr1[i].to_owned());
            i += 1;
        } else {
            result.push(arr2[j].to_owned());
            j += 1;
        }
    }

    while i < arr1.len() {
        result.push(arr1[i].to_owned());
        i += 1;
    }

    while j < arr2.len() {
        result.push(arr2[j].to_owned());
        j += 1;
    }

    result
}