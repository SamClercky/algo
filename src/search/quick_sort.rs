#[allow(unconditional_recursion)]
pub fn quick_sort<T: PartialOrd>(array: &mut [T]) {
    if array.len() == 0 {
        return
    }

    let pivot = array.len() - 1;

    let mut left = 0;
    let mut right = array.len() - 1;

    loop {
        // Search left
        while array[left] < array[pivot] {
            // if left >= array.len() - 1 {
            //     // is this necessary? NO, because guards at end
            //     break;
            // }
            left += 1;
        }

        // Search right
        while array[right] > array[pivot] {
            // if right == 0 {
            //     // is this necessary? NO, because guards at end
            //     break;
            // }
            right -= 1;
        }

        if left >= right {
            // break out of loop
            break;
        }

        array.swap(left, right);

        // make ready for next loop
        left = if left >= array.len() - 1 {
            break;
        } else {
            left + 1
        };

        right = if right == 0 {
            break;
        } else {
            right - 1
        };
    }

    array.swap(left, pivot); // Waarom?

    // Start recursion
    quick_sort(&mut array[..left]);
    quick_sort(&mut array[(left + 1)..]);
}
