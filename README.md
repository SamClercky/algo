# Algo

This repository contains a library in which I try to implement as much as possible
different algorithms in the rust programming language.

## Included algorithms

### 1. Sorting
* Selection sort
* Bubble sort
* Quick sort
* Merge sort (normal)
* Merge sort (recursive)

## Disclaimer
I am learning and this repository is a way to exercise these algorithms. So this
library is not meant to be used in performance critical environments, but is intended
as a learning platform.
